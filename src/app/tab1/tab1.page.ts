import { Component } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Chart } from 'chart.js';
import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: [ 'tab1.page.scss' ]
})
export class Tab1Page {

  mdl_country: string = '';
  data_countries: any = [];

  constructor(
    public admobFree: AdMobFree,
    private http: HTTP,
  ) { }

  ngOnInit() {
    this.load_admob();
  }

  ionViewDidEnter() {
    this.countries();
    this.chart();
  }


  load_admob() {
    const bannerConfig: AdMobFreeBannerConfig = {
      isTesting: false,
      id: 'ca-app-pub-3302992149561172/1655704474',
      autoShow: true
    };
    this.admobFree.banner.config(bannerConfig);

    this.admobFree.banner.prepare().then(() => {
    }).catch(e => console.log(e));
  }

  countries() {
    let url = 'https://restcountries.eu/rest/v2/all';
    this.http.get(url, {}, {}).then(res => {
      console.log(res.data);

      res.data = JSON.parse(res.data);
      this.data_countries = res.data;

    }).catch(error => {
      console.log(error.error);
    });
  }

  chart() {

    let url = 'https://coronavirus-tracker-api.herokuapp.com/v2/latest';
    this.http.get(url, {}, {}).then(res => {
      console.log(res.data);
      res.data = JSON.parse(res.data);

      var ctx = document.getElementById('pieChart');
      var pieChart = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: [
            'Confirmed : ' + res.data.latest.confirmed,
            'Deaths : ' + res.data.latest.deaths,
            'Recovered : ' + res.data.latest.recovered
          ],
          datasets: [ {
            label: '# of Votes',
            data: [ res.data.latest.confirmed, res.data.latest.deaths, res.data.latest.recovered ],
            backgroundColor: [
              '#3498db',
              '#e74c3c',
              '#2ecc71',
            ],
            borderWidth: 0
          } ]
        },
        options: {
          legend: {
            labels: {
              boxWidth: 14,
              fontColor: '#fff',
              fontSize: 16,
            },
            position: 'right',
          }
        }
      });
    }).catch(error => {
      console.log(error.error);
    });
  }

  selectCountry() {
    let url = 'https://coronavirus-tracker-api.herokuapp.com/v2/locations?country_code=' + this.mdl_country + '&timelines=1';
    // let url = 'https://coronavirus-tracker-api.herokuapp.com/v2/locations?country_code=' + 'IT' + '&timelines=1';
    this.http.get(url, {}, {}).then(res => {
      console.log(res.data);

      res.data = JSON.parse(res.data);

      //alert(JSON.stringify(res.data.locations[ 0 ].timelines.confirmed.timeline))

      //PIE CHART
      var ctx = document.getElementById('pieChart');
      var pieChart = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: [
            'Confirmed : ' + res.data.latest.confirmed,
            'Deaths : ' + res.data.latest.deaths,
            'Recovered : ' + res.data.latest.recovered
          ],
          datasets: [ {
            label: '# of Votes',
            data: [
              res.data.latest.confirmed,
              res.data.latest.deaths,
              res.data.latest.recovered ],
            backgroundColor: [
              '#3498db',
              '#e74c3c',
              '#2ecc71',
            ],
            borderWidth: 0
          } ]
        },
        options: {
          legend: {
            labels: {
              boxWidth: 14,
              fontColor: '#fff',
              fontSize: 16,
            },
            position: 'right',
          },
        }
      });


      //-----DATES-----
      let date_array: any = JSON.stringify(res.data.locations[ 0 ].timelines.confirmed.timeline);

      date_array = date_array.replace('{', '').replace('}', '');
      date_array = date_array.split('"').join('');

      date_array = date_array.split(',');

      for (let i = 0; i < date_array.length; i++) {
        date_array[ i ] = date_array[ i ].substring(0, 10);
      }


      //-----CONFIRMED-----
      let timeline_confirmed: any = JSON.stringify(res.data.locations[ 0 ].timelines.confirmed.timeline);

      timeline_confirmed = timeline_confirmed.replace('{', '').replace('}', '');

      timeline_confirmed = timeline_confirmed.split('"2020').join('');
      timeline_confirmed = timeline_confirmed.split('T00:00:00Z":').join('');

      for (let i = 0; i <= 31; i++) {
        timeline_confirmed = timeline_confirmed.split('-0' + i).join('');
      }

      for (let i = 10; i <= 31; i++) {
        timeline_confirmed = timeline_confirmed.split('-' + i).join('');
      }

      timeline_confirmed = timeline_confirmed.split(',');

      //-----DEATHS-----
      let timeline_deaths: any = JSON.stringify(res.data.locations[ 0 ].timelines.deaths.timeline);

      timeline_deaths = timeline_deaths.replace('{', '').replace('}', '');

      timeline_deaths = timeline_deaths.split('"2020').join('');
      timeline_deaths = timeline_deaths.split('T00:00:00Z":').join('');

      for (let i = 0; i <= 31; i++) {
        timeline_deaths = timeline_deaths.split('-0' + i).join('');
      }

      for (let i = 10; i <= 31; i++) {
        timeline_deaths = timeline_deaths.split('-' + i).join('');
      }

      timeline_deaths = timeline_deaths.split(',');

      //-----RECOVERED-----
      let timeline_recovered: any = JSON.stringify(res.data.locations[ 0 ].timelines.recovered.timeline);

      timeline_recovered = timeline_recovered.replace('{', '').replace('}', '');

      timeline_recovered = timeline_recovered.split('"2020').join('');
      timeline_recovered = timeline_recovered.split('T00:00:00Z":').join('');

      for (let i = 0; i <= 31; i++) {
        timeline_recovered = timeline_recovered.split('-0' + i).join('');
      }

      for (let i = 10; i <= 31; i++) {
        timeline_recovered = timeline_recovered.split('-' + i).join('');
      }

      timeline_recovered = timeline_recovered.split(',');




      var ctx = document.getElementById('lineChart');

      var lineChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: date_array,
          datasets: [ {
            label: 'Confirmed',
            backgroundColor: '#3498db',
            borderColor: '#3498db',
            data: timeline_confirmed,
            fill: false,
          },
          {
            label: 'Deaths',
            backgroundColor: '#e74c3c',
            borderColor: '#e74c3c',
            data: timeline_deaths,
            fill: false,
          },
          {
            label: 'Recovered',
            backgroundColor: '#2ecc71',
            borderColor: '#2ecc71',
            data: timeline_recovered,
            fill: false,
          } ]
        },
        options: {
          responsive: true,
          title: {
            display: false,
            text: 'Chart'
          },
          tooltips: {
            mode: 'index',
            intersect: false,
          },
          hover: {
            mode: 'nearest',
            intersect: true
          },
          legend: {
            labels: {
              boxWidth: 14
            }
          },
          scales: {
            xAxes: [ {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Month'
              }
            } ],
            yAxes: [ {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Numbers'
              }
            } ]
          }
        }
      });

    }).catch(error => {
      console.log(error.error);
      alert(error.error)
    });
  }

}
